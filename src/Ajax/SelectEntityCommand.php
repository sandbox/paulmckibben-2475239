<?php
/**
 * Created by PhpStorm.
 * User: paul
 * Date: 4/17/15
 * Time: 9:14 AM
 */

namespace Drupal\er_modal_select\Ajax;


use Drupal\Core\Ajax\CommandInterface;

class SelectEntityCommand implements CommandInterface {
  /**
   * The CSS ID for the entity reference field.
   *
   * @var string
   */
  protected $fieldId;


  /**
   * An array containing the entity_id and label for the selected entity.
   *
   * @var array
   */
  protected $entityInfo;

  /**
   * Create an instance of the SelectEntityCommand.
   *
   * @param string $fieldId
   *   CSS ID of entity reference field.
   * @param array $entityInfo
   *   Array containing the entity_id and label for the selected entity.
   */
  public function __construct($fieldId, array $entityInfo) {
    $this->fieldId = $fieldId;
    $this->entityInfo = $entityInfo;
  }

  /**
   * Implements \Drupal\Core\Ajax\CommandInterface::render().
   */
  public function render() {
    return array(
      'command' => 'select_entity',
      'fieldId' => $this->fieldId,
      'entityInfo' => $this->entityInfo,
    );
  }
}