<?php
/**
 * @file
 * Contains Drupal\er_modal_select\SelectWidget\SelectWidgetBase.
 */

namespace Drupal\er_modal_select\SelectWidget;

use Drupal\Core\Form\FormInterface;
use Drupal\Core\Form\FormStateInterface;

abstract class SelectWidgetBase implements FormInterface {

  /**
   * Form state of the field widget form.
   *
   * @var \Drupal\Core\Form\FormStateInterface
   */
  protected $form_state;

  /**
   * The field ID of entity being referenced.
   *
   * @var string
   */
  protected $fieldId;

  /**
   * The bundle info for the entity being referenced.
   *
   * @var array
   */
  protected $bundleInfo;

  /**
   * The field settings for the entity reference field.
   * @var array
   */
  protected $fieldSettings;

  /**
   * The entity class.
   * @var \Drupal\Core\Entity\EntityTypeInterface
   */
  protected $entityDefinition;

  /**
   * Constructor.
   *
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state.
   * @param string $fieldId
   *   The ID of the entity reference field.
   * @param array $fieldSettings
   *   The settings for the entity reference field.
   */
  public function __construct(FormStateInterface $form_state, $fieldId, $fieldSettings) {
    $this->form_state = $form_state;
    $this->fieldId = $fieldId;
    $this->fieldSettings = $fieldSettings;
    $target_type = $this->fieldSettings['target_type'];
    $this->bundleInfo = \Drupal::entityManager()->getBundleInfo($target_type);
    $this->entityDefinition = \Drupal::entityManager()->getDefinition($target_type);
  }

  /**
   * Get the target entity type ID.
   *
   * @return string
   *   The target entity type ID.
   */
  public function getTargetEntityTypeId() {
    return $this->fieldSettings['target_type'];
  }

  /**
   * Get a list of allowed bundles for the entity reference field.
   *
   * @return array
   *   Maps bundle ID to readable bundle name.
   */
  public function getAllowedBundles() {
    if ($this->getTargetEntityTypeId() != 'node') {
      return NULL;
    }
    $bundles = [];
    foreach($this->bundleInfo as $key => $values) {
      if (empty($this->fieldSettings['handler_settings']['target_bundles']) ||
          in_array($key, $this->fieldSettings['handler_settings']['target_bundles'])) {
        $bundles[$key] = $values['label'];
      }
    }
    return $bundles;
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    // Acts as a marker for Ajax calls.
    $form['er_modal_select_field_id'] = [
      '#type' => 'hidden',
      '#value' => $this->fieldId,
    ];
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    // No validation.
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    // No submit logic.
  }
}