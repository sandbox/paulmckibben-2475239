/**
 * @file
 * Provides javascript behavior for Entity Reference Modal Select field widget.
 */
(function ($, Drupal, drupalSettings) {

  Drupal.AjaxCommands.prototype.select_entity = function (ajax, response, status) {
    var field = $("[id^=" + response.fieldId + "]");
    var targetId = field.find('input.erms-target-id');
    var updateButton = field.find('input.erms-ajax-update');

    // Update the hidden target ID.
    targetId.val(response.entityInfo.entity_id);

    // Trigger the hidden button to cause the field form to update.
    window.setTimeout(function(){
      updateButton.trigger('update_entity');
    }, 1);
  };

}(jQuery, Drupal, drupalSettings));
